---
layout: post
title: "Fundstücke 32/2015"
date: 2015-08-09 21:27:24 +0200
comments: true
categories: Fundstücke
---

[Homme de Plume: What I Learned Sending My Novel Out Under a Male Name](http://jezebel.com/homme-de-plume-what-i-learned-sending-my-novel-out-und-1720637627)  
"Total data: George sent out 50 queries, and had his manuscript requested
17 times. He is eight and a half times better than me at writing the
same book. Fully a third of the agents who saw his query wanted to see
more, where my numbers never did shift from one in 25."

[Beyond PEP 8 -- Best practices for beautiful intelligible code - PyCon 2015](https://www.youtube.com/watch?v=wf-BqAjZb8M)  
Aufschlussreicher Vortrag, der aufzeigt wie man besser verständlichen
Python-Code schreiben kann. Die ersten ca. 20 Minuten hätte man zwar
durchaus etwas kürzen können, aber danach wird es richtig interessant.

[Deap Vally - Baby I Call Hell](https://www.youtube.com/watch?v=wUXVi3l-GUg)
