---
layout: post
title: "BitTorrent Sync auf QNAP-NAS-Geräten"
date: 2013-05-21 17:31
comments: true
categories: Linux
---

[Dropbox](http://db.tt/RfeVRSy) (Referral-Link) ist an sich eine schöne
Sache. Einfach Dateien in einen Ordner werfen, und auf jedem Gerät, das
man mit seinem Dropbox-Account verbindet, wird dieser Ordner
synchronisiert. Das erlaubt mir beispielsweise, überall auf die Dateien
zuzugreifen, die ich für die Uni brauche. Aber Dropbox hat auch
Nachteile. Es ist mit einem Dollar pro Jahr und Gigabyte recht teuer.
Außerdem hat Dropbox jederzeit Zugriff, auf die Daten, die man ihnen
anvertraut.

Und hier kommt [BitTorrent Sync](http://labs.bittorrent.com/experiments/sync.html)
ins Spiel. Das Programm erlaubt es, beliebig große Ordner zwsichen
beliebig vielen Rechnern verschlüsselt über das BitTorrent-Protokoll zu
synchronisieren. BitTorrent Sync ist zwar erst in der Alpha-Phase, läuft
aber bereits recht stabil.

Die Synchronisierung zwischen zwei Rechnern funktioniert natürlich nur,
wenn beide Rechner gleichzeitig eingeschaltet sind. Um eine mit Dropbox
vergleichbare Funktionalität zu bekommen, braucht man also einen Server,
der rund um die Uhr eingeschaltet ist, oder zumindest immer dann, wenn
einer der anderen Rechner läuft. Dafür kommt beispielsweise ein
[NAS-Server](http://de.wikipedia.org/wiki/Network_Attached_Storage) in
Frage, wie sie etwa von [QNAP](http://www.qnap.com/) hergestellt werden.

Und damit kommen wir zum eigentlichen Inhalt dieses Artikels. Ich hatte
am Pfingst-Wochenende etwas Zeit zum Experimentieren und habe BitTorrent
Sync auf meinem QNAP-NAS zum Laufen bekommen. Eine Anleitung und die
benötigten Skripte sind unter <https://github.com/puppe/btsync-qpkg> zu
finden.
