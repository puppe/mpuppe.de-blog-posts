---
layout: post
title: "Fundstücke 28/2015"
date: 2015-07-05 12:14:29 +0200
comments: true
categories: Fundstücke
---

## Griechenland

Die jetzige Zuspitzung der Krise in und um Griechenland ist nur ein
Symptom viel tiefer liegendender Probleme unseres Wirtschaftssystems.
Man könnte vieles zu dem Thema schreiben. Vielleicht mache ich das auch
irgendwann. Hier erst mal nur Links, ohne Kommentare.

["Die Politik hat sich ins Gefängnis der Märkte begeben"](http://www.sueddeutsche.de/politik/krise-in-griechenland-die-politik-hat-sich-ins-gefaengnis-der-maerkte-begeben-1.2538360)

[Gregor Gysi: Der Prohpet](http://le-bohemien.net/2015/07/03/gregor-gysi-der-prophet/)

["Volksbefragung": EU entsetzt über neuartige Entscheidungsmethode aus Griechenland](http://www.der-postillon.com/2015/06/volksbefragung-eu-entsetzt-uber.html)

[“Wenn es ernst wird, muss man lügen”](http://blog.zeit.de/herdentrieb/2015/07/02/wenn-es-ernst-wird-muss-man-luegen_8554)

## Sonstiges

[Inspeqtor and OSS Products With Mike Perham](http://5by5.tv/changelog/130)  
Mike Perham hat zwei Open-Source-Projekte gestartet, von denen er
mittlerweile ganz gut leben kann. Genau genommen, verdient er sein Geld
mit proprietären Zusatzfeatures.  [In dem Artikel](http://www.linux.com/news/software/applications/831018-how-to-make-money-from-open-source-platforms),
den ich [letzte Woche](/blog/2015/06/28/fundstucke-27-slash-2015/)
verlinkt habe, wurde dieses Modell als "Open Core" bezeichnet.

[“Kein Aserbaidschaner würde je behaupten, in einer Demokratie zu
leben”](http://erscheinungsraum.de/er033-kein-aserbaidschaner-wuerde-je-behaupten-in-einer-demokratie-zu-leben/)  
Ich wusste bisher sehr wenig über Aserbaidschan. Jetzt weiß ich mehr.

[The Future of UI Design? Old-School Text Messages](http://www.wired.com/2015/06/future-ui-design-old-school-text-messages/) und [On conversational UIs](http://interconnected.org/home/2015/06/16/conversational_uis)  
"Conversational UIs" sind anscheinend ein großer Trend in China. Ich bin
skeptisch. Künstliche Intelligenz ist eben meistens doch nicht
intelligent genug. Aber es schadet nicht über Alternativen
nachzudenken. Kennt jemand Beispiele für solche "Apps", die man
hierzulande mal ausprobieren könnte?

[The Lone Bellow - Button](https://www.youtube.com/watch?v=ahyIALLgUN4)
