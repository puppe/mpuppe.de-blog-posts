---
layout: post
title: "Warum Octopress?"
date: 2012-02-23 11:16
comments: true
categories: Octopress
description: "Vor- und Nachteile von Octopress"
keywords: "octopress vorteile, octopress nachteile"
---

Die Auswahl an Software zum Hosten eines Weblogs ist groß. Zum einen gibt es die Spezialisten wie bspw. [Wordpress](http://wordpress.org/) oder [Serendipity](http://www.s9y.org/), zum anderen ist fast jedes der [unzähligen Content-Management-Systeme](http://en.wikipedia.org/wiki/Comparison_of_content_management_systems) mehr oder weniger gut für Blogs geeignet. Und nicht zuletzt bieten Dienste wie [Blogger](http://www.blogger.com/) und (wieder) [Wordpress](http://wordpress.com/) schlüsselfertig eine kostenlose Webpräsenz.

Ich habe mich nach einiger Überlegung für [Octopress](http://octopress.org) entschieden. Octopress ist kein Content-Management-System im eigentlichen Sinne. Man erstellt seine Blogposts lokal in einer simplen Textdatei mit einem beliebigen Editor. Die Formatierung erfolgt mittels [Markdown](http://de.wikipedia.org/wiki/Markdown). Octopress generiert aus diesen Dateien die Website. Das Ergebnis besteht fast ausschließlich aus HTML und Javascript. Und hier wird klar, was Octopress so einzigartig macht: **Es gibt keine serverseitigen Abhängigkeiten.** Datenbanksystem, PHP, Python, Ruby etc., alles nicht notwendig!

## Vorteile

Daraus ergeben sich eine Reihe interessanter Vorteile:

- *Freie Wahl der Plattform*: Da es keine Abhängigkeiten gibt, kann man ein Octopress-Blog quasi überall hosten. Ich nutze beispielsweise im Moment [Github Pages](http://pages.github.com/). Eine interessante Alternative ist sicher auch [Amazon S3](http://www.ianwootten.co.uk/2011/09/09/hosting-an-octopress-blog-on-amazon-s3). Nicht zuletzt macht das Octopress zu einer ziemlich *günstigen* Angelegenheit. Denn ein bisschen Webspace inklusive .de-Domain ist schon für unter zwei Euro monatlich zu haben.
- *Einfache Migration*: Um mit einem Octopress-Blog auf einen anderen Server zu ziehen, reicht einfaches Kopieren der Dateien.
- *Relativ einfach aufzusetzen*: Dazu verweise ich auf die [offizielle Anleitung](http://octopress.org/help/). Mit ein paar einfachen Schritten hat man schnell eine "Entwicklungsumgebung". Für die Grundkonfiguration reicht es Basis-URL, Titel, Untertitel und Autor anzugeben.
- *Einfache Bedienung*: Mit `rake new_post["title"]` wird ein neuer Post erstellt. `rake preview` erzeugt die fertige Website und startet praktischerweise gleich einen Webserver unter http://localhost:4000. Änderungen an den Quelldateien werden umgehend übernommen. Um die Änderungen schließlich ins Netz zu laden, reicht ein einfaches `rake generate && rake deploy`.

## Nachteile

Nach all dem Lob, möchte ich die Nachteile aber nicht verschweigen:

- Da ein Octopress-Blog statisch ist, ist man für eine *Kommentarfunktion* auf [Disqus](http://disqus.com/) angewiesen. Auch die *Suche* muss über einen externen Anbieter laufen.
- Vielleicht hat der Leser es bereits bemerkt: Man sollte sich schon ein wenig wohl fühlen im *Terminal*[^terminal]. Letztlich ist das Ganze aber relativ schmerzlos. Man muss eigentlich nur den Anleitungen folgen. Ist Octopress erst mal aufgesetzt, reichen für das Erstellen neuer Inhalte eine Handvoll Befehle.
- Man sollte sich jeweils eine halbe Stunde mit [Git](http://git-scm.com/) und [RVM](http://beginrescueend.com/) auseinandersetzen, um zu verstehen, was man tut.[^rvm]
- Um sein Blog komplett auf Deutsch zu übersetzen, sind noch einige [manuelle Anpassungen](http://www.uru.ch/blog/2011/09/12/octopress-oberflaeche-im-schnelldurchgang-auf-deutsch-uebersetzen/) notwendig. Hier würde ich mir wünschen, dass eine einzelne Änderung in der Konfigurationsdatei ausreicht.

[^terminal]:
	Übertriebene Angst vor der Kommandozeile ist meiner Meinung nach ohnehin unbegründet.

[^rvm]:
	Das kann in beiden Fällen aber ein echter Gewinn auch für andere Projekte sein.
