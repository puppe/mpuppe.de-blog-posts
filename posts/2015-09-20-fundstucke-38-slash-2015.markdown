---
layout: post
title: "Fundstücke 38/2015"
date: 2015-09-20 19:37:42 +0200
comments: true
categories: Fundstücke
---

[Why Futurism Has a Cultural Blindspot](http://nautil.us/issue/28/2050/why-futurism-has-a-cultural-blindspot)  
'“Futurology is almost always wrong,” the historian Judith Flanders suggested to me, “because it rarely takes into account behavioral changes.” And, she says, we look at the wrong things: “Transport to work, rather than the shape of work; technology itself, rather than how our behavior is changed by the very changes that technology brings.” It turns out that predicting who we will be is harder than predicting what we will be able to do.'

[Inside Amazon: Wrestling Big Ideas in a Bruising Workplace](http://www.nytimes.com/2015/08/16/technology/inside-amazon-wrestling-big-ideas-in-a-bruising-workplace.html)  
Ich werde mich in Zukunft vermehrt nach Alternativen zu Amazon umsehen.

[Alternativlos, Folge 35](http://alternativlos.org/35/)  
"In Alternativlos Folge 35 reden wir über Diskurs- und Debattenkultur und die Facebook-Löschdebatte."

[Aynotchesh Yererfu by The Budos Band](https://thebudosband.bandcamp.com/track/aynotchesh-yererfu)
