---
layout: post
title: "Fundstücke 27/2015"
date: 2015-06-28 21:20:01 +0200
comments: true
categories: Fundstücke
---

Statt gelegentlich Links kommentarlos über Twitter zu verteilen, will
ich in Zukunft hier im Blog mehr oder weniger (eher weniger) regelmäßig
auf interessante Artikel, Vorträge, Musik etc. verweisen.

[The Tradeoff Fallacy](https://www.asc.upenn.edu/sites/default/files/TradeoffFallacy_1.pdf) (PDF, via [TechCrunch](http://techcrunch.com/2015/06/06/the-online-privacy-lie-is-unraveling/))  
Es wird ja gerne behauptet, dass Konsumenten gerne ihre Daten abgeben,
um im Gegenzug kostenfreie Dienste zu erhalten. Diese Studie belegt,
dass eine Mehrheit der Konsumenten keineswegs mit diesem Deal
einverstanden ist, sondern sich einfach machtlos fühlt. Meine Meinung:
Unternehmen wie Facebook sitzen dank [Netzwerkeffekt](https://de.wikipedia.org/wiki/Netzwerkeffekt) auf einem Monopol, das sie ausnutzen, um die Konditionen zu diktieren. Natürlich könnte ich auf Whatsapp verzichten und einen anderen Messenger nutzen, aber da sind eben nicht die Leute, die ich erreichen will.

[The Internet's Original Sin](http://www.theatlantic.com/technology/archive/2014/08/advertising-is-the-internets-original-sin/376041/)  
Der Artikel verweist auch auf einen [großartigen Vortrag von Maciej Cegłowski](http://idlewords.com/bt14.htm). Werbefinanzierte Geschäftsmodelle führen notwendigerweise zu
Zentralisierung und Überwachung.

[A Constructive Look At TempleOS](http://www.codersnotes.com/notes/a-constructive-look-at-templeos)  
Es wäre leicht, TempleOS als eine Kuriosität abzutun. Immerhin behauptet
der Programmierer, von Gott beauftragt worden zu sein, ein
Betriebssystem zu entwickeln. Aber statt billige Witze zu machen, geht
der Autor ohne Vorurteile an die Sache heran. Und er stellt fest, dass
TempleOS tatsächliche einige coole Ideen beinhaltet.

[The Death Of The Von Neumann Architecture](http://www.codersnotes.com/notes/the-death-of-the-von-neumann-architecture)  
Beim Lesen des Artikels musste ich direkt an [Cory Doctorows Vortrag auf dem 28C3](http://media.ccc.de/browse/congress/2011/28c3-4848-en-the_coming_war_on_general_computation.html) denken.
Es geht um Kontrolle und Macht. Wenn ich einen Computer kaufe, gehört er
dann mir, oder dem Hersteller? [Wenn ich ein Auto kaufe, gehört es dann mir oder dem Hersteller?](http://www.slate.com/blogs/future_tense/2015/05/22/gm_and_john_deere_say_they_still_own_the_software_in_cars_customers_buy.html)

[How to Make Money from Open Source Platforms](http://www.linux.com/news/software/applications/831018-how-to-make-money-from-open-source-platforms) [Teil 2](http://www.linux.com/news/software/applications/831645-how-to-make-money-from-open-source-platforms-part-2) [Teil 3](https://www.linux.com/news/software/applications/833312-how-to-make-money-from-open-source-platforms-part-3) [Teil 4](https://www.linux.com/news/software/applications/833939-how-to-make-money-from-open-source-platforms-part-4)  
Kann man mit Freier Software Geld verdienen? Vor allem Teil 3 fand ich
aufschlussreich.

[Artificial intelligence?](http://radar.oreilly.com/2015/06/artificial-intelligence.html)  
"We need to be aware of our prejudices and bigotries, and make sure that we don’t build systems that grant our prejudices the appearance of science. That’s ultimately why AI scares us: we worry that it will be as inhuman as humans all too frequently are. If we are aware of our own flaws, and can honestly admit and discuss them, we’ll be OK." "We might use AI to hide from our responsibility for our mistakes, but they will most certainly be our mistakes."

[TIS-100](http://www.zachtronics.com/tis-100/) und [Door Kickers](http://inthekillhouse.com/doorkickers/)  
Zwei tolle Spiele! Vielleicht muss man Programmierer sein, um mit
TIS-100 Spaß zu haben. Andererseit hätte ich selbst niemals gedacht, dass ich mal freiwillig
Assembler programmieren würde.

[9 Anti-Patterns Every Programmer Should Be Aware Of](http://sahandsaba.com/nine-anti-patterns-every-programmer-should-be-aware-of-with-examples.html)  
Besonders über Punkt 3 ("Analysis Paralysis") stolpere ich leider gelegentlich.

[The World Was Never Closer To Nuclear War Than On Jan. 25, 1995](http://www.businessinsider.com/the-world-has-never-been-closer-to-nuclear-war-than-it-was-during-this-1995-event-2012-8?IR=T)

[Endangered Blood: NPR Music Tiny Desk Concert](https://www.youtube.com/watch?v=3rHD1_kUC5E)
