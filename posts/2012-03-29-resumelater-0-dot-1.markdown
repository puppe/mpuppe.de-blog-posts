---
layout: post
title: "resumeLater 0.1 veröffentlicht"
date: 2012-03-29 11:53
comments: true
categories: resumeLater
description: resumeLater 0.1 veröffentlicht
keywords: resumelater, youtube
---

{% img right /images/posts/2012-03-29_resumeLater.svg 200 resumeLater %}

resumeLater ist eine Firefox-Erweiterung. Sie erlaubt es, die aktuelle Position in einem Youtube-Video zu speichern. Später kann die Wiedergabe an exakt der selben Stelle fortgesetzt werden. Das ist vor allem praktisch bei längeren Videos (etwa von Vorträgen), die man nicht an einem Stück schauen möchte. Download bei [addons.mozilla.org](https://addons.mozilla.org/en-US/firefox/addon/resumelater/).

In Version 0.1 wird die Video-Liste nur lokal im Browser gespeichert. Für Version 1.0 ist die Möglichkeit geplant, die Videos mit Bookmarking-Sites wie [Delicious](http://delicious.com) und [Pinboard](http://pinboard.in) zu synchronisieren. Mittelfristig ist auch eine Android-App denkbar.

Außerdem vorgesehen:

 * Unterstützung weiterer Video-Seiten ([Vimeo](http://vimeo.com/), [Dailymotion](http://www.dailymotion.com/video/xpqx51) etc.)
 * Deaktivieren bei Private Browsing

Quellcode und Issue-Tracker auf [Github](https://github.com/puppe/resumeLater).