---
layout: post
title: "Fundstücke 35/2015"
date: 2015-08-31 12:21:17 +0200
comments: true
categories: Fundstücke
---

[Momo](https://de.wikipedia.org/wiki/Momo_%28Roman%29)  
Der Roman ist 1973 erschienen, also bin ich mit meiner Empfehlung etwas
spät dran. Das Buch hat aber nichts von seiner Aktualität verloren, eher
im Gegenteil. Man findet das Werk zwar meist in der Kinder- und
Jugendbuchabteilung, aber gerade auch für Erwachsene gibt es hier viele
Denkanstöße. Was ist Zeit und wie gehen wir damit um?

[Momo, Dogen. and the Commodification of Time](http://ccbs.ntu.edu.tw/FULLTEXT/JR-MISC/101783.htm)  
"This essay will explore the deep resonances between Ende's view of time
in Momo and the Buddhist perspective on time, particularly as expressed
by the Japanese Zen master Dogen (1200 - 1253). These resonances are of
more than literary or historical interest: understanding what Ende and
Dogen have to say about time gives us important insight into how we
experience time today."

[The Cat Empire Live at AB - Ancienne Belgique (Full concert)](https://www.youtube.com/watch?v=eiY3xMM4DSk)
