---
layout: post
title: "Fundstücke 37/2015"
date: 2015-09-15 18:39:48 +0200
comments: true
categories: Fundstücke
---

[The Inspection Paradox is Everywhere](http://allendowney.blogspot.de/2015/08/the-inspection-paradox-is-everywhere.html)  
Viele schöne Beispiele zu einem statistischen Paradoxon.

[“Und wenn du einfach unter einem Männernamen schreibst?”](https://zoebeck.wordpress.com/2015/08/22/und-wenn-du-einfach-unter-einem-maennernamen-schreibst/)  
Die Autorin bezieht sich auf [einen Artikel](http://localhost:4000/blog/2015/08/09/fundstucke-32-slash-2015/),
den ich hier [vor ein paar Wochen](http://mpuppe.de/blog/2015/08/09/fundstucke-32-slash-2015/)
auch schon mal verlinkt hatte.

[TSA Master Keys](https://www.schneier.com/blog/archives/2015/09/tsa_master_keys.html)  
Unter anderem deswegen sind staatlich vorgeschriebene Backdoors eine sehr,
sehr dumme Idee.

[The Bottom Of The Well](http://www.npr.org/sections/money/2015/07/22/425392169/episode-640-the-bottom-of-the-well)  
Stellvertretend für viele spannende Episoden des Podcasts "Planet Money".

[WR470 Nachgefragt: Kurdistan](http://www.wrint.de/2015/08/30/wr470-nachgefragt-kurdistan/)  
Holger Klein spricht mit Enno Lenze über dessen jüngste Reise nach
Kurdistan.

[Caravan](https://www.youtube.com/watch?v=TS-G4UQTfUo)
