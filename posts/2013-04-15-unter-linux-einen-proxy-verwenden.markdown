---
layout: post
title: "Unter Linux einen Proxy verwenden"
date: 2013-04-15 21:11
comments: true
categories: Linux
description: "Wie man unter Linux einen Proxy-Server verwendet, aber dennoch auf bestimmte Rechner direkt zugreift."
keywords: "linux, proxy, ausnahmen, no_proxy"
---

Manchmal ist man in Netzwerken unterwegs, deren Firewall erst mal
jeglichen Netzwerkverkehr blockt. Um den Benutzern dennoch zu
ermöglichen, zumindest auf das Web zuzugreifen, wird dann meist ein
Proxy-Server zur Verfügung gestellt, der zwischen den Benutzern und dem
eigentlichen Ziel der Anfragen vermittelt.

Nehmen wir einmal an, dass der Proxy-Server unter der IP-Adresse
192.168.0.42 erreichbar ist und auf Port 8080 lauscht. Dann muss in die
Datei `/etc/profile` folgendes eingetragen werden:

``` bash
export http_proxy=http://192.168.0.42:8080
export https_proxy=http://192.168.0.42:8080
export ftp_proxy=http://192.168.0.42:8080
export HTTP_PROXY=$http_proxy
export HTTPS_PROXY=$https_proxy
export FTP_PROXY=$ftp_proxy
```

Was aber, wenn man für manche Anfragen explizit *keinen* Proxy-Server
verwenden will, z.B. weil man auf Dienste zugreifen will, die auf dem
eigenen Rechner oder innerhalb des lokalen Netzwerkes laufen? Dazu dient
die Umgebungsvariable `no_proxy`. Möchte man beispielsweise für Anfragen
an `localhost` oder die IP-Adresse 192.168.0.10 keinen Proxy verwenden,
so sollte der Datei `/etc/profile` folgende Zeile hinzugefügt werden:

``` bash
export no_proxy=localhost,192.168.0.10
```


