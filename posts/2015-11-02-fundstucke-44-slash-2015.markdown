---
layout: post
title: "Fundstücke 44/2015"
date: 2015-11-02 23:04:37 +0100
comments: true
categories: Fundstücke
---

[Automatic Face Recognition and Surveillance](https://www.schneier.com/blog/archives/2015/10/automatic_face_.html)  
"We need limitations on how our images can be collected without our
knowledge or consent, and on how they can be used. The technologies
aren't going away, and we can't uninvent these capabilities. But we can
ensure that they're used ethically and responsibly, and not just as a
mechanism to increase police and corporate power over us."

[Schaffen wir das?](http://www.zeit.de/gesellschaft/zeitgeschehen/2015-10/fluechtlinge-fischer-im-recht/komplettansicht)  
'Die Formel "nach Maßgabe der Möglichkeiten" ist also, als
verfassungsrechtliche Formel, die Verweisung entweder auf ein
intellektuelles Nichts oder auf nichts als die politische Willkür. Und
sonst wirklich nichts!'

[Hardcore History 56 Kings of Kings](http://www.dancarlin.com/product/hardcore-history-56-kings-kings/)  
Eine neue Folge von Dan Carlins "Hardcore History".

[Frazey Ford - Done](https://www.youtube.com/watch?v=PXRrySTujn8)  
Neulich bei [detektor.fm](https://detektor.fm/) entdeckt.
