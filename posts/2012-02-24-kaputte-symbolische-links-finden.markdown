---
layout: post
title: "Kaputte symbolische Links finden"
date: 2012-02-24 11:21
comments: true
categories: Bash
description: So lassen sich kaputte symbolische Links finden.
---

Mit folgendem Terminalbefehl lassen sich kaputte symbolische Links finden:

``` bash
find / -type l ! -exec test -r {} \; -print
```

Quelle: [http://www.linuxforums.org/forum/programming-scripting/94116-how-find-dead-symbolic-links.html#post468681](http://www.linuxforums.org/forum/programming-scripting/94116-how-find-dead-symbolic-links.html#post468681)
