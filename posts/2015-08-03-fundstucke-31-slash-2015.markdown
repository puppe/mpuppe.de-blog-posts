---
layout: post
title: "Fundstücke 31/2015"
date: 2015-08-03 17:25:43 +0200
comments: true
categories: Fundstücke
---

Nach mehrwöchiger Pause gibt es nun neue Fundstücke von mir.

[Are You Living in a Computer Simulation?](http://www.simulation-argument.com/simulation.html)  
"This paper argues that at least one of the following propositions is true: (1) the human species is very likely to go extinct before reaching a “posthuman” stage; (2) any posthuman civilization is extremely unlikely to run a significant number of simulations of their evolutionary history (or variations thereof); (3) we are almost certainly living in a computer simulation. It follows that the belief that there is a significant chance that we will one day become posthumans who run ancestor-simulations is false, unless we are currently living in a simulation. A number of other consequences of this result are also discussed."

[Encryption's holy grail is getting closer, one way or another](http://www.zdnet.com/article/encryptions-holy-grail-is-getting-closer-one-way-or-another/)  
"The ultimate goal -- encryption's holy grail, some have called it -- is something called fully homomorphic encryption, where the entire system works on encrypted data, and returns an encrypted result. The only point in the process where data would be decrypted would be when the user wanted to see the result, and that would presumably happen in the application or client software, not in the database server in the cloud."

[What is the Truck Factor of Popular GitHub
Applications? A First Assessment (PDF)](https://peerj.com/preprints/1233v1.pdf)  
Leider hängt Software oft von einzelnen Entwicklern ab. Was passiert,
wenn diesen etwas zustößt? Bei freier Software hat man immerhin noch den
Quellcode.

[Moving Fast With High Code Quality](https://engineering.quora.com/Moving-Fast-With-High-Code-Quality)  
In dem Artikel wird beschrieben, wie die Leute bei Quora für gute
Code-Qualität sorgen.

[So ein Europäer will ich gar nicht sein](http://kiezneurotiker.blogspot.de/2015/07/so-ein-europaer-will-ich-gar-nicht-sein.html)  
"Das Europa 2015 ringt mir keinen Pathos mehr ab. Es steht nur noch für die Herrschaft der Buchhalter, für die Ablehnung demokratischer Instrumente wie Abstimmungen, für das Mobben legitimer Regierungen, das Europa 2015 steht für tausende ersoffene Flüchtlinge im Mittelmeer, Überwachung, Militarisierung, Freihandel zugunsten der globalisierten Konzerne, [...]" Ich selbst bin ähnlich enttäuscht von Europa.

[Einfach. Gut. Leben.](http://itsyour.life/einfach-gut-leben/)  
Ich finde die Idee des Minimalismus sehr ansprechend. Mittlerweile
überlege ich mir jede Anschaffung drei mal. Zeug und Konsum machen nicht
glücklich.

[ident.me: Your Address as a Service](http://api.ident.me/)  
Mit ``curl ident.me ; echo`` kann man sich ganz einfach seine
öffentliche IP-Adresse anzeigen lassen.

[Jon Spencer Blues Explosion - Get Your Pants On (Live on KEXP)](https://www.youtube.com/watch?v=i45CNVx7e8Y)
