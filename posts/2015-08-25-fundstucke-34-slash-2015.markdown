---
layout: post
title: "Fundstücke 34/2015"
date: 2015-08-25 17:05:05 +0200
comments: true
categories: Fundstücke
---

[Reportage: Drei Brüder aus Syrien in der Trierer Aufnahmeeinrichtung für Asylbegehrende](http://lokalo.de/artikel/95112/reportage-drei-brueder)  
Ich finde den Artikel aus zwei Gründen bemerkenswert: die Reportage ist
ein positives Beispiel für Lokaljournalismus, und hier wird *mit*
Flüchtlingen statt nur *über* sie gesprochen.

[The Architecture of Open Source Applications: ZeroMQ](http://www.aosabook.org/en/zeromq.html)  
Dieses Kapitel aus [The Architecture of Open Source Applications](http://aosabook.org)
gibt einen guten Überblick über [ZeroMQ](http://zeromq.org/). Das war
für mich ganz hilfreich, weil ich ZeroMQ eventuell bald für ein Projekt
einsetzen werde. Übrigens kann ich [die ganze Reihe](http://www.aosabook.org/)
empfehlen. Ich habe zwar bisher nur einzelne Kapitel gelesen, die waren
aber allesamt sehr lehrreich.

[Waterboarding für den gemeingefährlichen Irren! Deutsche Journalisten über Claus Weselsky](http://www.stefan-niggemeier.de/blog/21153/waterboarding-fuer-den-gemeingefaehrlichen-irren-deutsche-journalisten-ueber-claus-weselsky/)  
Erschreckend.

[Web Design: The First 100 Years](http://idlewords.com/talks/web_design_first_100_years.htm)  
"The Internet is full of projects big and small whose defining trait is
that they came out of nowhere and captured people's imaginations. It's
also full of awesome cat videos. The key part of this vision is that the
Internet succeeds by remaining open and participatory. No one acts as
gatekeeper, and it is not just a channel for mindless consumption."
([Weitere Vorträge von Maciej Cegłowski](http://idlewords.com/talks/))

[Confronting New Madrid](http://idlewords.com/2015/07/confronting_new_madrid.htm)  
Maciej Cegłowski ist durch die [New Madrid Seismic Zone](https://en.wikipedia.org/wiki/New_Madrid_Seismic_Zone) gereist und schreibt darüber.
