---
layout: post
title: "Fundstücke 39/2015"
date: 2015-09-27 21:54:44 +0200
comments: true
categories: Fundstücke
---

[How to Write a Git Commit Message](http://chris.beams.io/posts/git-commit/)  
Ein paar gute Hinweise.

[What Happens Next Will Amaze You](http://idlewords.com/talks/what_happens_next_will_amaze_you.htm)  
"State surveillance is driven by fear. And corporate surveillance is
driven by money.  The two kinds of surveillance are as intimately
connected as tango partners. They move in lockstep, eyes rapt, searching
each other's souls. The information they collect is complementary. By
defending its own worst practices, each side enables the other.
Today I want to talk about the corporate side of this partnership."

[Zitat am Freitag: Was wirklich gegen Erkältung hilft](https://buggisch.wordpress.com/2015/09/18/zitat-am-freitag-was-wirklich-gegen-erkaeltung-hilft/)  
tl;dr: Ausreichend schlafen beugt Erkältungen vor.
