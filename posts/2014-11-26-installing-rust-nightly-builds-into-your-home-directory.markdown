---
layout: post
title: "Installing Rust nightly builds into your home directory"
date: 2014-11-26 23:53:52 +0100
comments: true
categories: Rust
description: "How to install Rust into your home directory"
keywords: "rust, rust lang, home directory, sudo, root"
---

Currently, the easiest way to get up and running with
[Rust](http://www.rust-lang.org/) is to run the following command in
your shell:

    curl -s https://static.rust-lang.org/rustup.sh | sudo sh

This will install Rust into `/usr/local` and you usually need root
permissions to do that. I had been looking for an alternative for two
reasons:

* On my Macbook, `/usr/local` is mostly managed by
  [Homebrew](http://brew.sh/) and `brew doctor` complains if it finds
  libraries that were put there by someone else.
* I don't have root permissions at the university computer lab.

Fortunately, installing Rust into `$HOME` is relatively painless.
`rustup.sh` lets you specify a custom prefix. The above command only has
to be slightly tweaked:

    curl -s https://static.rust-lang.org/rustup.sh | sh -s -- --prefix=$HOME/.local

Once Rust has been installed, there's still three things left to do.

1. Put `rustc` etc. on your $PATH.
2. Tell `rustc` where to find the Rust libraries.
3. Tell `man` where to find the manual pages.

The first two points can be accomplished by adding the following to your
`$HOME/.bashrc` or `$HOME/.zshrc`:

``` bash
if [ -d $HOME/".local/bin" ] ; then
    export PATH="$HOME/.local/bin:$PATH"
fi

rust_dyld=$HOME/.local/lib/rustlib/x86_64-apple-darwin/lib
if [ ! -d $rust_dyld ] ;
    rust_dyld=$HOME/.local/lib/rustlib/x86_64-unknown-linux-gnu/lib
fi

if [ -d $rust_dyld ] ; then
    if [ -z $DYLD_LIBRARY_PATH ] ; then
        export DYLD_LIBRARY_PATH=$rust_dyld
    else
        export DYLD_LIBRARY_PATH=$rust_dyld:$DYLD_LIBRARY_PATH
    fi
fi

unset rust_dyld
```

Note: Take a look at lines 5 to 8. You should check whether either of
these two directories actually exists. If not, you have to modify those
lines accordingly.

If you are using [fish](http://fishshell.com/), put this into
`$HOME/.config/fish/config.fish` instead:

``` plain
if test -d $HOME/.local/bin
    set -gx PATH $HOME/.local/bin $PATH
end

set -l rust_dyld $HOME/.local/lib/rustlib/x86_64-apple-darwin/lib
if test ! -d $rust_dyld
    set rust_dyld $HOME/.local/lib/rustlib/x86_64-unknown-linux-gnu/lib
end

if test -d $rust_dyld
    set -gx DYLD_LIBRARY_PATH $rust_dyld $DYLD_LIBRARY_PATH
end
```

Now we need to tell `man` where to find the manual pages. Add the
following line to `$HOME/.manpath`:

    MANPATH_MAP /Users/martin/.local/bin    /Users/martin/.local/share/man

Finally, start a new terminal session and try the following:

    rustc --version
    man rustc

If you later want to upgrade to the latest nightly, just rerun
`rustup.sh` (like above):

    curl -s https://static.rust-lang.org/rustup.sh | sh -s -- --prefix=$HOME/.local
