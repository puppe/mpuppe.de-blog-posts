---
layout: post
title: "Fundstücke 33/2015"
date: 2015-08-17 22:23:48 +0200
comments: true
categories: Fundstücke
---

[With Windows 10, Microsoft sells you out](http://jasonlefkowitz.net/2015/07/with-windows-10-microsoft-sells-you-out/)  
"[...] **your interest in how your computer works and Microsoft’s interest are no
longer aligned.** While you will want your computer to do things quickly
and efficiently and unobtrusively, Microsoft will want it to do those
things slowly and clunkily and painfully; because every delay, every
useless dialog box you have to click through, is another opportunity for
them to show you an ad. And while you will want your computer to keep
your secrets secret, you won’t be able to trust Microsoft to want the
same thing anymore, because suddenly all those secrets are worth money
to them. They can use them to match you up even more exquisitely with
advertisers, who have become Microsoft’s real customers for Windows."

[Fix Windows 10](https://fix10.isleaked.com/)  
Immerhin erlaubt Windows 10 recht genau einzustellen, welche Daten an
Microsoft gesendet werden. Hier wird das ausführlich mit Screenshots
erklärt.

[Documentation at scale: The principles](http://250bpm.com/blog:58)  
"1. Acknowledge that brute force doesn't work. 2. Make documentation a first
class citizen. 3. Make documentation executable. 4. Track the intent. 5. Measure it."


[Ein Abgrund von Landesverrat](http://www.zeit.de/gesellschaft/zeitgeschehen/2015-08/pressefreiheit-netzpolitik-fischer-im-recht/komplettansicht)  
Bundesrichter Thomas Fischer schreibt in seiner Kolumne über den Fall
#landesverrat. Dabei kommt seine Meinung sehr deutlich durch. Auch wenn
man diese nicht vollständig teilt, ist der Artikel sehr interessant zu
lesen.

[Lee Fields and the Expressions - "Faithful Man" (Live at WFUV)](https://www.youtube.com/watch?v=4ZibJKnW6gI)
